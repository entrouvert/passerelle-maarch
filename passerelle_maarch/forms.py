# passerelle-maarch
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.utils.text import slugify
from django import forms

from .models import MaarchManagement

class MaarchManagementForm(forms.ModelForm):
    class Meta:
        model = MaarchManagement
        exclude = ('slug', 'users')

    def save(self, commit=True):
        if not self.instance.slug:
            self.instance.slug = slugify(self.instance.title)
        return super(MaarchManagementForm, self).save(commit=commit)

class MaarchManagementUpdateForm(MaarchManagementForm):
    class Meta:
        model = MaarchManagement
        exclude = ('users',)
