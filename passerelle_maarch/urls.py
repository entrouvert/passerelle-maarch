# passerelle-maarch
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import patterns, include, url
from passerelle.urls_utils import decorated_includes, required, app_enabled
from django.contrib.auth.decorators import login_required

from .views import *

public_urlpatterns = patterns('',
    url(r'^(?P<slug>[\w,-]+)/$', MaarchManagementDetailView.as_view(),
        name='maarch-view'),
    url(r'^(?P<slug>[\w,-]+)/ping/$', MaarchPingView.as_view(),
        name='maarch-ping'),
    url(r'^(?P<slug>[\w,-]+)/resource/$', MaarchResourceView.as_view(),
        name='maarch-resource'),
    url(r'^(?P<slug>[\w,-]+)/resource/(?P<table_name>[\w_-]+)/(?P<resource_id>\d+)/$', MaarchResourceView.as_view(),
        name='maarch-resource-id'),
    url(r'^(?P<slug>[\w,-]+)/resource/(?P<table_name>[\w_-]+)/(?P<adr_table_name>[\w_-]*)/(?P<resource_id>\d+)/$', MaarchResourceView.as_view(),
        name='maarch-resource-id-adr'),
    url(r'^(?P<slug>[\w,-]+)/contact/$', MaarchContactView.as_view(),
        name='maarch-contact'),
    url(r'^(?P<slug>[\w,-]+)/contact/(?P<contact_id>\d+)/$', MaarchContactView.as_view(),
        name='maarch-contact-id'),
)

management_urlpatterns = patterns('',
    url(r'^add$', MaarchManagementCreateView.as_view(),
        name='maarch-add'),
    url(r'^(?P<slug>[\w,-]+)/edit$', MaarchManagementUpdateView.as_view(),
        name='maarch-edit'),
    url(r'^(?P<slug>[\w,-]+)/delete$', MaarchManagementDeleteView.as_view(),
        name='maarch-delete'),
)

urlpatterns = required(
    app_enabled('passerelle_maarch'),
    patterns('',
        url(r'^maarch/', include(public_urlpatterns)),
        url(r'^manage/maarch/',
            decorated_includes(login_required, include(management_urlpatterns))),
    )
)
